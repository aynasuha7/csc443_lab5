<!DOCTYPE html>
<html>
<body>

<?php
$monthDays = array('Splorch'     =>23,  'Sploo'       =>28,
                   'Splat'       => 2,  'Splatt'      =>3,
                   'Spleen'      =>44,  'Splune'      =>30,
                   'Spling'      =>61,  'Slendo'      =>61,
                   'Sploctomber' =>31,  'Splictember' =>31,
                   'Splanet'     =>30,  'TheRest'     =>22);

                   echo " Number of days in the shortest month: " . min($monthDays) . "</br";
                   echo " Name of the shortest month: " . array_search(min($monthDays),$monthDays) . "</br>";
                   echo " Total number of days in a year: " . array_sum($monthDays);
?>

</body>
</html>